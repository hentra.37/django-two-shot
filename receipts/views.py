from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from receipts.models import Account, ExpenseCategory, Receipt
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.user_property = self.request.user
        item.save()
        return redirect("home")


class CategoryView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"
    context_object_name = "expensecategory"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"
    context_object_name = "accounts"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
