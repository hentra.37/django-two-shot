from django.urls import path
from django.contrib.auth import views as auth_views
from receipts.views import (
    AccountView,
    CategoryView,
    ReceiptCreateView,
    ReceiptListView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", CategoryView.as_view(), name="expense_list"),
    path("accounts/", AccountView.as_view(), name="accounts_list"),
]
